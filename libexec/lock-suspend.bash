source libexec/lock-screen.bash

__lock_suspend__quiet () {
  $* &> /dev/null &
}

export -f __lock_suspend__quiet

__lock_suspend () {
  sudo -v \
    && __lock_suspend__quiet __lock_screen \
    && sudo systemctl suspend
}

export -f __lock_suspend
