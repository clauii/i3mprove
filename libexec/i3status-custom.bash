__i3status_custom__unouno_quota () {
  local unouno_quota
  read unouno_quota 2> /dev/null < /var/run/1und1/quota
  printf '%s\n' "${unouno_quota}"
}

export -f __i3status_custom__unouno_quota

__i3status_custom__foo () {
  local foo
  # read foo 2> /dev/null < /foo
  printf '%s\n' "${foo}"
}

export -f __i3status_custom__foo

__i3status_custom__original_line () {
  local original_line
  read original_line
  printf '%s\n' "${original_line}"
}

export -f __i3status_custom__original_line

__i3status_custom () {
  local version_line
  local wrapper_line

  i3status \
    | {
      read version_line
      echo "${version_line}"
      read wrapper_line
      echo "${wrapper_line}[]"

      while true; do
        __i3status_custom__original_line
        __i3status_custom__unouno_quota
        __i3status_custom__foo
      done \
        | stdbuf -o 0 sed -E -e 's/^,?//' \
        | {
            jq -cMR --unbuffered \
              '
                def color($percentage):
                  if ($percentage) > 80 then
                    { color: "#FF0000" }
                  elif ($percentage) > 60 then
                    { color: "#FFFF00" }
                  else
                    null
                  end;

                [., input, input]
                  as [
                    $original_line,
                    $unouno_quota,
                    $foo
                  ]
                |
                [
                  if ($unouno_quota | length) > 0 then
                    {
                      name: "quota",
                      instance: "/var/run/1und1/quota",
                      markup: "none",
                      full_text: "1&1: \($unouno_quota)"
                    }
                      + (
                        color($unouno_quota | scan("^\\d+")
                          | tonumber?) // null
                      )
                  else
                    empty
                  end,
                  if ($foo | length) > 0 then
                    {
                      name: "foo",
                      instance: "foo",
                      markup: "none",
                      full_text: "Foo: \($foo)"
                    }
                      + (
                        color($foo | scan("^\\d+")
                          | tonumber?) // null
                      )
                  else
                    empty
                  end
                ]
                  + ($original_line | fromjson)
              '
          } \
        | stdbuf -o 0 sed -E -e 's/^/,/'
    }
}

export -f __i3status_custom
