__xremap () {
  local windowname="${1?}"
  local classname="${2?}"

  xdotool \
    search --name "${windowname}" \
    set_window --classname "${classname}" \
    windowunmap --sync \
    windowmap --sync
}

export -f __xremap
