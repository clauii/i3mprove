__xwaitfor () {
  local windowname="${1?}"

  for d in $(seq 1 256); do
    xdotool search --name "${windowname}" && return
    sleep 1
  done
}

export -f __xwaitfor
